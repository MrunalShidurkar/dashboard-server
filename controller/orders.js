let ordersData = require('../db/orders')
let random = require('generate-random-data');
let _ = require('lodash');
let io = require('../app').io

let Months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

module.exports = {
    new: function (order) {
        return new Promise((resolve, reject) => {
            order = order || this.createRandomOrder();
            ordersData.push(order)
            this.publish(ordersData)
            resolve(order)
        })
    },
    startIntervalFor10Seconds: function () {
        let interval = setInterval(() => {
            let order = this.createRandomOrder()
            ordersData.push(order);
            this.publish(ordersData)
        }, 1000)
        setTimeout(function () { clearInterval(interval); }, 10000);
    },
    publish: function (newOrdersData = ordersData) {
        let currentDate = new Date();
        let currentYear = currentDate.getFullYear();
        let groupByYear = this.groupBy(newOrdersData);

        //ordersByYear
        let ordersByYear = _.map(groupByYear, (orders, key) => [key, orders.length])
        io.emit('ordersByYear', ordersByYear)

        //current Year
        let currentYearOrder = groupByYear[currentYear];
        let orderByMonths = _.groupBy(currentYearOrder, function (order) {
            let date = new Date(order.date)
            let monthNum = date.getMonth()
            return Months[monthNum]
        });
        orderByMonths = this.sortByMonth(orderByMonths)
        io.emit('orderByMonths', orderByMonths)

        //status
        let orderByStatus = _.groupBy(currentYearOrder, function (order) {
            return order.status
        });
        orderByStatus = _.map(orderByStatus, (orders, key) => [this.getStatusName(key), orders.length])
        io.emit('orderByStatus', orderByStatus)

    },
    createRandomOrder: () => ({
        id: random.id(),
        name: {
            first: random.maleFirstName(),
            last: random.lastName()
        },
        email: random.email("foodexchange.com"),
        phone: random.mobile(),
        date: new Date(),
        status: String(random.int(0, 2))
    }),
    getStatusName: function (key) {
        switch (key) {
            case "0": return 'Delivered';
            case "1": return 'Not delivered';
            case "2": return 'Cancelled';
        }
    },
    groupBy: function (ordersData, fnName = "getFullYear") {
        return _.groupBy(ordersData, function (order) {
            let date = new Date(order.date)
            return date[fnName] ? date[fnName]() : date.getFullYear()
        });
    },
    sortByMonth: function (data) {
        let currentDate = new Date();
        let currentMonth = currentDate.getMonth()
        let monthsTillCurrentMonth = Months.slice(0, currentMonth + 1)
        return _.map(monthsTillCurrentMonth, monthName => {
            return [monthName, (data[monthName] && data[monthName].length) || 0]
        })
    }
}