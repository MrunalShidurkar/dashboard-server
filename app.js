let express = require('express');
let socket = require('socket.io');
let path = require('path');
let cookieParser = require('cookie-parser');
let http = require('http');
let debug = require('debug')('app:server');
let cors = require('cors')
let app = express();
let logger = require('./helpers/logger');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())



let port = normalizePort(process.env.PORT || '3001');
app.set('port', port);
let server = http.createServer(app);
server.listen(port, () => {
    logger.info("Server started")
});

server.on('error', onError);
server.on('listening', onListening);

let count = 0;
setInterval(() => count += 1, 1000)
let io = socket(server);
io.on('connection', (socket) => {
    console.log(socket.id);
    setInterval(() => io.emit('count', count), 1000)
});

function normalizePort(val) {
    let port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    let bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    let addr = server.address();
    let bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

module.exports = { app, io };
let indexRouter = require('./routes/index');
let ordersRouter = require('./routes/orders');
app.use('/', indexRouter);
app.use('/orders/', ordersRouter);


