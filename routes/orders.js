let express = require('express');
let router = express.Router();
let orders = require('../controller/orders');
let logger = require("../helpers/logger");
router.get('/publish', function (req, res, next) {
    orders.publish();
    res.status(200).send({ message: 'Subscribed successfully' })
});
router.post('/new', function (req, res, next) {
    orders.new()
        .then(data => {
            logger.info("Order added");
            res.status(200).send({ message: 'Order added' });
        })
        .catch(err => {
            logger.error(err);
            res.status(500).send(err);
        })
});
router.post('/intervalfor10', function (req, res, next) {
    orders.startIntervalFor10Seconds();
    logger.info("Interval started");
    res.status(200).send({ message: "Interval started" });
});
module.exports = router;
